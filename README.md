# This is an instruction for assembling crawler robot
<img alt="My Robot" src="./images/my.png" width="640"/>

## Getting started

Here we are going to assemble quadruped robot step by step.
Parts are available on my etsy store.\
Also, this instruction will be useful for developing your own robot.

At first, we should print all these details:

- case_bottom (1)
- case_mid (1)
- top_case (1)
- connector_1_l (2)//only for mg90s
- connector_1_r (2)//only for mg90s
- connector_1 (4)//only for mg92b
- connector_2 (4)
- connector_3 (4)
- cover_bat (1)
- cover_for_legs (2)
- foot_tpu (4)
- inner (4)
- inner_0 (8)

See an image for details\
<img alt="My Robot" src="./images/image1.png" width="640"/>
<img alt="My Robot" src="./images/image2.png" width="640"/>


Also we need next parts:
- screws M2x8 or M2x10 (36)
- screws M2x6 (12)
- screws M2x4 (for display) (4-6)
- screws M3x6 (4)

Electrical parts:
- Raspberry Pi Zero WH (1)
- Henge Ubec 4A (6A ok also) (1)
- pca9685 (1)
- case for 18650x2 (1)
- 18650 battery (2)
- mg92b (or mg90s) (12)
- wires for connect batteries
- wires for breadboard
- mpu9250 sensor (1)
- ultrasonic sensor (1)
- lcd display (1)

Electronic scheme:

<img alt="My Robot" src="./images/scheme.png" width="640"/>

## Preparation for assembling

At first we need a solder or a small drill to make holes in some parts.

In this part we will do:
- assemble robot case and install motors into case
- assemble each leg
- prepare case for batteries into robot case
- move all motor's wires through holes into case and connect to pca9685

### Assembling of case

Install 'case_mid' on 'case_bottom' and use 4 M3x6 screws to assemble them together.

<img alt="My Robot" src="./images/image3.png" width="640"/>

Additionally, install four motors, like it is shown on image, and use 8 M2x8 screws.\
Use solder or drill to make small holes for them. Before it, put a screw for motor place, warm it for a few seconds to make small a hole,\
or use a drill with thin bit (less then 2 mm).

### Assembling of leg

First, wear resin foot_tpu part on connector_1
<img alt="My Robot" src="./images/image4.png" width="200"/>

Install 2 motors in connector_2 as shown on image, and make holes use solder or drill (as before)

Notice 1 - you can put wire from one motor through space inside connector_2.\
Notice 2 - see on image, we need 2 mirror assembling of leg (left/right)!

<img alt="My Robot" src="./images/image5.png" width="640"/>


Then, assemble 'cover_for_legs' into case using screws from complect of motor (they should be included).\
<img alt="My Robot" src="./images/image6.png" width="640"/>

When it is ready, get 'connector_3' details and put them.\
Do the same with 'connector_1'. See an image for details.

<img alt="My Robot" src="./images/image16.png" width="440"/>

Last step here is to use 'inner' and 'inner_0'. First, put screws M2x8 through the hole in this parts (you can use solder or drill to make them comfortable for it),
and then assemble it:
- 'inner' put through holes on connectors 1 and 3.
- 'inner_0' put through holes on the bottom on connector_3.

<img alt="My Robot" src="./images/image17.png" width="340"/>\
<img alt="My Robot" src="./images/image18.png" width="340"/>\
<img alt="My Robot" src="./images/image19.png" width="340"/>


### Prepare case for batteries

At first here, we should use wires for battery's connection

Use one wire as shown in image. Solder it.\
<img alt="My Robot" src="./images/image7.png" width="340"/>

Then, put this case in bottom part of robot's case, and use drill or solder to make holes for screws.\
Do not assemble this with a robot case at now, we will do it a bit later.\
<img alt="My Robot" src="./images/image8.png" width="340"/>

### Move all wires through holes into robot case

As shown on image, move all wires through this holes and connect them to pca9685.\
Let use this names for legs: leftArm, rightArm, leftArmBack, rightArmBack.

For them, connect as in this table:

| leg          | motorA | motorB | motorC |
| :------------ | :---- | :---- | :---- |
| leftArm      | 0 | 1 | 2 |
| rightArm     | 4 | 5 | 6 |
| leftArmBack  | 8 | 9 | 10 |
| rightArmBack | 12 | 13 | 14 |


<img alt="My Robot" src="./images/image11.png" width="340"/>\
<img alt="My Robot" src="./images/image12.png" width="340"/>

## Assembling of electrical parts

In this part we will do:
- install power supply
- install battery's case into robot
- connect power supply to pca9685

### Install power supply

Let's bring Henge Ubec 4A module, it should be set up on 5V (should be by default). Then cut sockets from both sides (or use other parts if you have it).\
Solder input wires to '-' and '+' metal parts on your battery's case. Also I recommend add switcher in '+' wire.\
For it cut this wire and solder switcher here. It will be useful to on/off robot.\
Move wires from other side through rectangular hole in case (remove throttle, and then wear again)\
<img alt="My Robot" src="./images/image9.png" width="450"/>

### Install battery's case into robot

Connect case into robot using 2 screws M3x8. Also, use scotch tape to connect power supply near to this case.\
<img alt="My Robot" src="./images/image10.png" width="450"/>

### Connect power supply to pca9685

Use other wires from Ubec to connect both of them into pca9685 module.
Solder one additional RED wire to '+' wire from power supply. It should be wire with special connector to Raspberry pi pin.\
For it you can use standard breadboard wire, cut one of socket and solder it. See an image for details.\
In the end, use 1-2 screws to set pca9685 to case.

<img alt="My Robot" src="./images/image13.png" width="450"/>\
<img alt="My Robot" src="./images/image14.png" width="450"/>

## Installation of PC and programming part

After assembling we are ready to calibrate these legs and install Raspberry Pi Zero and other sensors.
What we will do:
- install soft on Raspberry Pi and software
- make zero positioning for legs and finish leg assembling
- install sensors
- run test code

### Installation on Raspberry Pi Zero

For this PC we will use Raspberian OS - [from official source](https://www.raspberrypi.com/software/operating-systems/)\
I use 'Raspberry Pi OS Lite' without desktop - it is required SSH terminal experience! If you are not familiar with it, install desktop.\
Also, CD card is 32Gb is ok for use.
After installation we should install all tools.

First, update and upgrade your system. Run this commands into terminal
```
sudo apt update
sudo apt upgrade
```

After updating, you should download node from official source. Of course you can choose other versions
[thank for this instruction](https://www.golinuxcloud.com/install-nodejs-and-npm-on-raspberry-pi/)
```
sudo wget https://nodejs.org/dist/v16.19.1/node-v16.19.1-linux-armv7l.tar.xz
sudo tar -xvf node-v16.19.1-linux-armv7l.tar.xz
cd node-v16.19.1-linux-armv7l
sudo cp -R * /usr/local
```

After this, run command 'node -v' and if it returns something like '16.19.1', everything is well.

When node has installed, you should clone this project in your folder (whenever you want)\
[https://gitlab.com/Vladimir_Glukhov/grobotics-local](https://gitlab.com/Vladimir_Glukhov/grobotics-local)

Then go to this folder and install all packages.
TO-DO:/ fixe this repo!!!
```
sudo su
git clone https://gitlab.com/Vladimir_Glukhov/grobotics-local
cd grobotics-local
npm install
```
Check if everything works correctly
```
npm start
```
No errors should be

After this we are ready for calibration

### Make zero positioning

Before, we should connect pca9685 board to Raspberry Pi.\
Connect this board to i2c pins on Raspberry,\
then connect power pin to +5V pin on raspberry\
<img alt="My Robot" src="./images/image15.png" width="450"/>

After this you can install 18650 batteries into case and run PC.

For zero positioning, we will use zero positioning scripts.\
To do this, go to project (grobotics-local) folder and go to 'setting' folder.\
In this folder run command in terminal
```
node zero_left_arm //zero_left_arm_back .. right_arm .. right_arm_back - for each leg diff. scripts
```

After zero-positioning we are ready to continue with assembling of leg\
For this, wear the connectors from servo motors complect on the gear in direction as show on image.\
Then use solder or drill to make a holes for screws. Make sure - firstly install screws in connectors and then warm them a bit, or instead of this use drill to make a small hole for screw. Do this for each leg AFTER zero positioning.
After zero positioning, set legs parts as an image.

<img alt="My Robot" src="./images/image20.png" width="450"/>\
Two motors are connected\
<img alt="My Robot" src="./images/image21.png" width="450"/>\
Example - Make a hole here\
<img alt="My Robot" src="./images/image22.png" width="450"/>\
After drill, screw it.

Do it for each leg.

After this all our robot almost ready!

### Install sensors into robot case

Into this robot can be installed:
- small led display
- ultrasonic sensor
- mpu9250 gyroscope

You can install led display into top case, use two M2x4 screws.

<img alt="My Robot" src="./images/image23.png" width="450"/>

Near is a place for Ultrasonic sensor.\
<img alt="My Robot" src="./images/image24.png" width="450"/>

On case_mid is a place for gyroscope mpu9250. There are a lot of different kinds sensor in the web, so, sensor can has different size.\
In this case just make additional holes in current place for your sensor.
<img alt="My Robot" src="./images/image25.png" width="450"/>

### Run test code

To run service, go to folder 'grobotics-local/service'

```
cd grobotics-local
cd service
node server
```

User your Raspberry IP address in browser or mobile phone\
You should see this:\
<img alt="My Robot" src="./images/interface.png" width="600"/>

Enjoy!
